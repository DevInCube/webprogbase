const mongoose = require('mongoose');

const express = require('express');
const app = express();

const databaseUrl = 'mongodb://localhost:27017/sample';
const serverPort = 3000;
const connectOptions = { useNewUrlParser: true };

mongoose.connect(databaseUrl, connectOptions)
    .then(() => console.log(`Database connected: ${databaseUrl}`))
    .then(() => app.listen(serverPort, () => console.log(`Server started: ${serverPort}`)))
    .catch(err => console.log(`Start error: ${err}`));