const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    login: { type: String, required: true },
    fullname: { type: String },
    created: { type: Date, default: Date.now },
});

const UserModel = mongoose.model('User', UserSchema);

class User {
    constructor(id, login, fullname) {
        this.id = id; // string
        this.login = login;  // string
        this.fullname = fullname;  // string
        // ...
    }
 
    // static functions to access storage
    // returns Promise with model by id or undefined
    static getById(id) { return Promise.reject(new Error("Not implemented")); }
    // returns Promise with an array of all models in storage
    static getAll() { 
        return UserModel.find({}).sort({ created: -1 });
    }
}
module.exports = User;