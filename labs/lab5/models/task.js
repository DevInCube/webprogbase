const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
    author: { type: mongoose.mongo.ObjectId, ref: "User" },
    name: { type: String, required: true },
    created: { type: Date, default: Date.now },
});

const TaskModel = mongoose.model('Task', TaskSchema);

class Task {

    constructor(id, name, authorId) {
        this.id = id; // string
        this.name = name;  // string
        this.author = authorId;  // string, reference to User instance
        // ...
    }

    // static asynchronous functions to access storage
    // returns Promise with model by id or undefined
    static getById(id) {
        return TaskModel.findOne({ _id: mongoose.ObjectId(id) })
            .populate('author')
            .exec();
    }
    // returns Promise with an array of all models in storage
    static getAll() {
        return TaskModel.find().sort({ created: -1 })
            .populate('author')
            .exec();
    }
    // returns new model id
    static insert(model) {
        return new TaskModel(model).save();
    }
    static update(model) {
        return TaskModel.findByIdAndUpdate(model.id, model);
    }
    static delete(id) { return Promise.reject(new Error("Not implemented")); }
};

module.exports = Task;